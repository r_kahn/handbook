---
title: "GitLab CI/CD - Hands-On Lab Overview"
description: "This Hands-On Guide walks you through the lab exercises in the GitLab CI/CD course."
---
# GitLab CI/CD Hands-On Guide

## Lab Guides

| Lab Name |  Lab Link |
|-----------|------------|
| Review example GitLab CI/CD section | [Lab Link](/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab1) |
| Create a project containing a `.gitlab-ci.yml` file and register a GitLab Runner | [Lab Link](/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab2) |
|  Create a basic CI configuration | [Lab Link](/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab3) |
| Display pipeline environment info | [Lab Link](/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab4) |
| Variable hierarchy | [Lab Link](/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab5) |
| Job policy pattern | [Lab Link](/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab6) |
|  Using artifacts |  [Lab Link](/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab7) |
| GitLab docker registry |  [Lab Link](/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab8) |
|  Security scanning | [Lab Link](/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab9) |
| Code quality scanning | [Lab Link](/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab9alt) |

## Quick links

Here are some quick links that may be useful when reviewing this Hands-On Guide.

* [GitLab CI/CD Course Description](https://about.gitlab.com/services/education/gitlab-ci/)
* [GitLab CI/CD Specialist Certification Details](https://about.gitlab.com/services/education/gitlab-cicd-associate/)

## Suggestions?

If you wish to make a change to the *Hands-On Guide for GitLab CI/CD*, please submit your changes via Merge Request!
